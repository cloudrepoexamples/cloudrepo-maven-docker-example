FROM atlassian/default-image:latest

#
# Copy the settings.xml over to the user's .m2 directory.
# If it's not root, update accordingly.
#
ADD settings.xml /root/.m2/settings.xml
