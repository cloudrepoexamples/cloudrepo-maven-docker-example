# Overview

This contains an example showing how to move a settings.xml file over to the Docker image.


## Commands

```
docker build -t example .
```

The above command builds the image.   To verify the installation of the settings.xml file, you can load the container with:

```
docker run -it example:latest /bin/bash

# From prompt run:
ls /root/.m2
```
